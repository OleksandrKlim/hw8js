"use strict";

let elem = document.querySelectorAll("p");
for (const i of elem) {
  i.style.backgroundColor = "#ff0000";
}
let optionsList = document.getElementById("optionsList");
console.log(optionsList);
console.log(optionsList.parentNode);
let children = optionsList.childNodes;
console.log(children);
for (const i of children) {
  console.log(`имя ${i.nodeName}`, `тип ${i.nodeType}`);
}
document.getElementById("testParagraph").innerHTML = "This is a paragraph";

const list = document.querySelectorAll(".main-header li");
console.log(list);
for (const li of list) {
  li.className = "nav-item";
  console.log(li);
}

let itemTest = document.querySelectorAll(".options-list-title");
console.log(itemTest);
for (const className of itemTest) {
  className.classList.remove("options-list-title");
  console.log(className);
}
